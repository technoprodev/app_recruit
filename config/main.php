<?php
$params['user.passwordResetTokenExpire'] = 3600;

$config = [
    'id' => 'app-recruit', 'name' => 'Recruit',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app-recruit\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-recruit',
        ],
        'user' => [
            'identityClass' => 'technosmart\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-recruitment', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-recruit',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

return $config;