<?php
namespace recruitment\controllers;

use Yii;
use recruitment\models\Applicant;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProfileController manages profile data for each logged applicant.
 */
class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    protected $category = [
        'basic',
        'physical',
        'contact',
        'address',
        'family',
        'document',
        'driving',
        'about-me',
    ];

    public function actionIndex($category)
    {
        // Yii::$app->debugx->debugx($this->category, $category, in_array($category, $this->category));
        if ($category == 'summary') {
            $model['profile'] = '';
            return $this->render('index', [
                'model' => $model,
                'title' => 'My Profile',
            ]);
        } else if(!in_array($category, $this->category))
            throw new \yii\web\HttpException(404, "Page not found.");

        $render = true;

        // init all models
        $user = Yii::$app->user->identity;
        $model['applicant'] = Applicant::findOne(['id_user' => $user->id]);
        // $model['child'] = $model['applicant']->child;

        // save all models & serve ajax validation
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            // load all user inputs into all models
            $model['applicant']->load($post);
            // $model['child']->load($post);

            // serve form validation result from/to ajax request
            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['applicant'])
                    // , ActiveForm::validate($model['child'])
                );
                return $this->json($result);
            }

            // modify models if needed, after input submission & before save
            // $model['applicant']->textinput = $model[applicant]->textinput . ' asdf';

            // make sure all models is safe to be saved
            if ($model['applicant']->validate() /* && $model['child']->validate() */) {
                // save all models one by one
                if ($model['applicant']->save(false)) {
                    // $model['child']->id_parent = $model[applicant]->id;
                    // if ($model['child']->save(false)) {
                        $render = false;
                    // }
                }
            }
        }

        // if new request / if saving all models fails
        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Edit ' . \yii\helpers\Inflector::titleize($category, true) . ' Information',
                'category' => $category
            ]);
        // if saving all models success
        else
            return $this->redirect(['index', 'id' => $model['applicant']->id]);
    }
}