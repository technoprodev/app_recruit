<?php
namespace recruitment\controllers;

use Yii;
use yii\web\Response;

use recruitment\models\Job;
use recruitment\models\JobCategory;


class RestController extends \technosmart\controllers\RestController
{
	
    public function actionJob($id = null,$limit = 1,$start = 0,$category="null"){
    	Yii::$app->response->format = Response::FORMAT_JSON;

    	$now = date("Y-m-d");
    	if($id){
    		$query = Job::find()->where(["id"=>$id])
	    						->all();
    	}else{
	    	$query = Job::find()/*->where(["and","date_start<='". $now ."'","date_end>'". $now ."'"])
	    						->orWhere(["and","date_start<'". $now ."'","date_end>='". $now ."'"])*/
	    						->offset($start)
	    						->limit($limit)
	    						->orderBy(["id" => SORT_DESC]);
	    	if($category!="null"){
	    		$query = $query->where(["id_job_category"=>$category]);
	    	}
	    	$query = $query->all();
    	}
    	return $query;
    }

    public function actionJobCategory($id = null){
    	Yii::$app->response->format = Response::FORMAT_JSON;

		$query = JobCategory::find()->all();

    	return $query;
    }
}
