<?php
namespace recruitment\controllers;

use Yii;
use recruitment\models\Job;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JobController implements the CRUD actions for Job model.
 */
class JobController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDatatables()
    {
        $db = Job::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('job');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'code',
                'name',
                'description',
                'requirement',
                'id_job_category',
                'batch',
                'date_start',
                'date_end',
                'id_job_step_last',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
            // Yii::$app->debug->debugx($query->createCommand()->sql);
        }
    }

    /**
     * If param(s) is null, creates new data(s) from model(s).
     * If all param(s) is not null, Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        $render = true;

        // only nulled all params or valued all params allowed
        $nulledParams = $valuedParams = false;
        foreach (func_get_args() as $key => $value) {
            if (is_null($value))
                $nulledParams = true;
            else
                $valuedParams = true;
        }
        if ($nulledParams && $valuedParams)
            throw new \yii\web\HttpException(404, "Page not found.");

        // init all models
        $model['job'] = $nulledParams ? new Job() : $this->findModel($id);
        // $model['child'] = $model['job']->child;

        // init value if new request
        if (Yii::$app->request->isGet && $model['job']->isNewRecord) {
            // $model[job]->id_combination = $this->sequence('dev-id_combination');
        }
        // save all models & serve ajax validation
        else if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            // load all user inputs into all models
            $model['job']->load($post);
            // $model['child']->load($post);

            // serve form validation result from/to ajax request
            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['job'])
                    // , ActiveForm::validate($model['child'])
                );
                return $this->json($result);
            }

            // modify models if needed, after input submission & before save
            // $model['job']->textinput = $model[job]->textinput . ' asdf';

            // make sure all models is safe to be saved
            if ($model['job']->validate() /* && $model['child']->validate() */) {
                // save all models one by one
                if ($model['job']->save(false)) {
                    // $model['child']->id_parent = $model[job]->id;
                    // if ($model['child']->save(false)) {
                        $render = false;
                    // }
                }
            }
        }

        // if new request / if saving all models fails
        if ($render)
            return $this->render('index', [
                'model' => $model,
                // 'title' => 'job',
            ]);
        // if saving all models success
        else
            return $this->redirect(['index', 'id' => $model['job']->id]);
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id = null)
    {
        // only nulled all params or valued all params allowed
        $nulledParams = $valuedParams = false;
        foreach (func_get_args() as $key => $value) {
            if (is_null($value))
                $nulledParams = true;
            else
                $valuedParams = true;
        }
        if ($nulledParams && $valuedParams)
            throw new \yii\web\HttpException(404, "Page not found.");

        // view all data
        if ($nulledParams) {
            return $this->render('list', [
                'title' => 'job',
            ]);
        }
        // view single data
        else {
            $model['job'] = $this->findModel($id);
            // $model['child'] = $model['job']->child;
            return $this->render('view-single', [
                'model' => $model,
                'title' => 'job',
            ]);
        }
    }

    /**
     * Deletes an existing Job model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Job model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Job the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Job::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
