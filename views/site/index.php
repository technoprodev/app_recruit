<?php

use  yii\helpers\Url;

$this->title = 'Home Page';

Yii::$app->debugx->debug(
    '<br>' .
    '@yii: ' . Yii::getAlias('@yii') . '<br>' .
    '@app: ' . Yii::getAlias('@app') . '<br>' .
    '@runtime: ' . Yii::getAlias('@runtime') . '<br>' .
    '@webroot: ' . Yii::getAlias('@webroot') . '<br>' .
    '@web: ' . Yii::getAlias('@web') . '<br>' .
    '@vendor: ' . Yii::getAlias('@vendor') . '<br>' .
    '@bower: ' . Yii::getAlias('@bower') . '<br>' .
    '@npm: ' . Yii::getAlias('@npm') . '<br>' .
    '@technosmart: ' . Yii::getAlias('@technosmart') . '<br>' .
    '@recruitment: ' . Yii::getAlias('@recruitment') . '<br>' .
    '@console: ' . Yii::getAlias('@console') . '<br>' .
    '__DIR__:' . __DIR__ . '<br>' .
    'dirname(__DIR__):' . dirname(__DIR__) . '<br>' .
    ''
);

Yii::$app->debugx->debug(
    '<br>' .
    "Url::to(['site/index']) : " . Url::to(['site/index']) . '<br>' .
    "Url::to(['index']) : " . Url::to(['index']) . '<br>' .
    "Url::to(['site']) : " . Url::to(['site']) . '<br>' .
    "Url::to(['asdf']) : " . Url::to(['asdf']) . '<br>' .
    "Url::to(['/']) : " . Url::to(['/']) . '<br>' .
    "Url::to(['']) : " . Url::to(['']) . '<br>' .
    "Url::to() : " . Url::to() . '<br>' .
    "Url::to(['dev/index']) : " . Url::to(['dev/index']) . '<br>' .
    "Url::to(['dev/form']) : " . Url::to(['dev/form']) . '<br>' .
    '<br>' .
    "Url::to(['site/index', 'id' => 1]) : " . Url::to(['site/index', 'id' => 1]) . '<br>' .
    "Url::to(['index', 'id' => 1]) : " . Url::to(['index', 'id' => 1]) . '<br>' .
    "Url::to(['site', 'id' => 1]) : " . Url::to(['site', 'id' => 1]) . '<br>' .
    "Url::to(['asdf', 'id' => 1]) : " . Url::to(['asdf', 'id' => 1]) . '<br>' .
    "Url::to(['/', 'id' => 1]) : " . Url::to(['/', 'id' => 1]) . '<br>' .
    "Url::to(['', 'id' => 1]) : " . Url::to(['', 'id' => 1]) . '<br>' .
    "Url::to() : " . Url::to() . '<br>' .
    "Url::to(['dev/index', 'id' => 1]) : " . Url::to(['dev/index', 'id' => 1]) . '<br>' .
    "Url::to(['dev/form', 'id' => 1]) : " . Url::to(['dev/form', 'id' => 1]) . '<br>' .
    ''
);

Yii::$app->debugx->debug(
    '<br>' .
    "php_sapi_name() : " . php_sapi_name() . '<br>' .
    "get_class(Yii::\$app) : " . get_class(Yii::$app) . '<br>' .
    ''
);

// Yii::$app->debugx->dd(technosmart\models\Menu::find()->where(['code' => 'recruitment-sidebar'])->asArray()->all());

?>