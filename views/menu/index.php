<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Menu';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('@web/technosmart/js/menu/index.js', ['depends' => 'technosmart\assets\JqueryUiAsset']);
$this->registerCssFile('@web/technosmart/css/menu/index.css', ['depends' => 'technosmart\assets\JqueryUiAsset']);

function menu_tree($list, $parent = null) {
    $html = null;

    foreach ($list as $menu) {
        if ($menu['parent'] == $parent) {
            if (!isset($menu['enable'])) continue;
            
            $child = menu_tree($list, $menu['id']);

            $html .= "<li class='"
                    . ($child === null ? 'open' : "has-submenu open")
                    . ($menu['enable'] ? null : "text-muted") . "' data-id='$menu[id]'>"
                        . "<a class='sortable-handle'>"
                            . ($parent ? "<i class='menu-icon'></i>" : null)
                            . "<i class='fa fa-arrows'></i>"
                            . "$menu[title]"
                        . "</a>"
                        . ($child === null ? "<ul></ul>" : $child)
                    . "</li>";
        }
    }

    if ($html === null)
        return null;

    if ($parent === null)
        return "<ul id='menu-sortable' class='menu-y menu-xs menu-fancy menu-border menu-text-gray menu-hover-text-azure submenu-border submenu-active-bg-light-blue submenu-hover-text-azure'>$html</ul>";
    else
        return "<ul>$html</ul>";
}

?>

<?php if (Yii::$app->controller->can('create')) : ?>
    <?= Html::a(
        '<i class="fa fa-plus"></i> Add Menu',
        ['create'],
        ['class' => 'btn border-azure margin-bottom-30'],
        ['modal'=>'']) ?>
<?php endif; ?>

<div class="sortable">
    <?= menu_tree($menus) ?> 
    <div class='loading'><?= Html::img("@web/technoart/asset/img/loader.gif") ?></div>
</div>