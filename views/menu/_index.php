<div class='pull-right'>"
                                .(Yii::$app->controller->can('update') ? 
                                    Html::a( // status
                                        $menu['enable'] ? "Aktif" : "Non-Aktif", 
                                        ["status", "id"=>$menu["id"]],
                                        [
                                            "class"=>[$menu['enable'] ? "label label-success" : "label label-danger"],
                                            "data-method" => "POST",
                                        ]
                                    ) : 
                                    "<span class='label ".($menu['enable'] ? "label-success" : "label-danger")."'>"
                                        .($menu['enable'] ? "Aktif" : "Non-Aktif")
                                    ."</span>"
                                )
                            ."</div>
                            <div class='pull-right parent-hover'>"
                                .(Yii::$app->controller->can('create') ? Html::a( // create
                                    "<i class='glyphicon glyphicon-plus'></i> Tambah", 
                                    ["create", "parent"=>$menu["id"]],
                                    ["modal"=>""]
                                ) : "")
                                .(Yii::$app->controller->can('update') ? Html::a( // update
                                    "<i class='glyphicon glyphicon-edit'></i> Edit", 
                                    ["update", "id"=>$menu["id"]],
                                    ["modal"=>""]
                                ) : "")
                                .(Yii::$app->controller->can('delete') ? Html::a( // delete
                                    "<i class='glyphicon glyphicon-remove'></i> Hapus", 
                                    ["delete", "id"=>$menu["id"]],
                                    [
                                        "data"=> [
                                            "confirm"=>"",
                                            "method"=>"post",
                                        ]
                                    ]
                                ) : "")
                            ."</div> <div class='clearfix'></div>