<?php

use technosmart\assets\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\yii\widgets\Alert;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app='main-app'>
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?= Yii::$app->params['app.description'] ?>">
        <meta name="keywords" content="<?= Yii::$app->params['app.keywords'] ?>">
        <meta name="author" content="<?= Yii::$app->params['app.author'] ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="base-url" content="<?= Yii::$app->getRequest()->getBaseUrl() ?>">
        <title><?= $this->title ? Html::encode($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/technoart/asset/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/technoart/asset/img/favicon.ico">
        <?php $this->head() ?>
    </head>

    <body class="ff-muli">
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <!-- START @WRAPPER -->
        <section id="wrapper">
            <!-- START @HEADER -->
            <header id="header" class="padding-y-10">
                <div id="header-left" class="text-center">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/technoart/asset/img/img-demo.png" alt="35x35" title="demo image 35x35" width="35px" class="img-circle"> <span class="ff-muli f-light fs-18">TECHNOSMART</span>
                </div>
                <div id="header-right" class="">
                    <form class="form-horizontal">
                        <div class="form-group margin-0">
                            <div class="col-sm-6 padding-0">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="form-search" name="form-search" placeholder="Search">
                                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                </div>
                            </div>
                             <!-- Html::a('Logout', ['logout'], ['class' => 'btn btn-default pull-right margin-right-20', 'data-method' => 'post']) ?> -->
                            <!-- <a href='<?= Yii::$app->urlManager->createUrl("site/logout") ?>' class="btn btn-default pull-right margin-right-20" data-method="post">Logout</a> -->
                        </div>
                    </form>
                </div>
            </header>
            <!-- /END @HEADER -->

            <!-- START @BODY -->
            <section id="body" class="has-sidebar-left bg-lighter">
                <!-- START @SIDEBAR LEFT -->
                <aside id="sidebar-left" class="bg-lightest">
                    <?php
                        $cache = Yii::$app->cache;
                        $key = 'menu-recruitment-sidebar';
                        if ($cache->exists($key)) {
                            $menus = $cache->get($key);
                        } else {
                            $menus = technosmart\models\Menu::find()->where(['code' => 'recruitment-sidebar'])->asArray()->all();
                            $cache->set($key, $menus);
                        }
                    ?>
                    <?= MenuWidget::widget([
                        'items' => Menu::menuTree($menus, null, true),
                        'options' => [
                            'class' => 'menu-y border-bottom menu-fancy menu-border menu-text-gray menu-active-bg-light-blue submenu-border menu-hover-darker-10 submenu-active-bg-light-blue',
                            'id' => 'menu-y',
                        ],
                        'activateItems' => true,
                        'openParents' => true,
                        'parentsCssClass' => 'has-submenu',
                        'encodeLabels' => false,
                        'labelTemplate' => '<a>{label}</a>',
                        'hideEmptyItems' => true,
                    ]); ?>
                </aside>
                <!-- /END @SIDEBAR LEFT -->

                <!-- START @PAGE WRAPPER -->
                <section id="page-wrapper">
                    <!-- START @PAGE SLIDER -->
                    <!-- <section id="page-slider">
                    </section> -->
                    <!-- /END @PAGE SLIDER -->

                    <!-- START @PAGE HEADER -->
                    <section id="page-header" class="bg-lightest">
                        <strong class="fs-18"><?= Html::encode($this->title) ?></strong>
                        <?php /*Breadcrumbs::widget([
                            'links' => $this->params['breadcrumbs'],
                        ])*/ ?>
                    </section>
                    <!-- /END @PAGE HEADER -->

                    <div class="fit-scroll">
                    <!-- START @PAGE BODY -->
                    <section id="page-body" ng-controller="allController">
                        <div class="bg-lightest padding-20 border shadow">
                            <?= Alert::widget() ?>
                            <?= $content ?>
                        </div>
                    </section>
                    <!-- /END @PAGE BODY -->

                    <!-- START @FOOTER -->
                    <footer id="footer" class="bg-lightest">
                        <span class="ff-muli f-light fs-18">TECHNOSMART</span>
                        About Press Copyright Creators Advertise Developers
                    </footer>
                    <!-- /END @FOOTER -->
                    </div>
                </section>
                <!-- /END @PAGE WRAPPER -->

                <!-- START @SIDEBAR RIGHT -->
                <!-- <aside id="sidebar-right">
                </aside> -->
                <!-- /END @SIDEBAR RIGHT -->
            </section>
            <!-- /END @BODY -->
        </section>
        <!-- /END @WRAPPER -->

    <?php $this->endBody(); echo "\n" ?>
    <script src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/technoart/asset/js/plugin-init.js"></script>
    <!-- <script src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/technosmart/plugin/angular/angular.min.js"></script>
    <script src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/technosmart/plugin/angular/angular-initial-value.min.js"></script> -->
    <script src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/technosmart/js/technosmart.js"></script>
    </body>
</html>
<?php $this->endPage() ?>