<?php

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/technosmart/js/job/list.js', ['depends' => 'technosmart\assets\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th>ID</th>
            <th>Code</th>
            <th>Name</th>
            <th>Description</th>
            <th>Requirement</th>
            <th>Id Job Category</th>
            <th>Batch</th>
            <th>Date Start</th>
            <th>Date End</th>
            <th>Id Job Step Last</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; ID" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Code" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Name" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Description" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Requirement" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Id Job Category" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Batch" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Date Start" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Date End" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Id Job Step Last" class="form-control no-border fs-12 f-normal"/></th>
        </tr>
    </thead>
</table>