<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?= Html::a('Update', ['update', 'id' => $model['job']->id], ['class' => 'btn btn-primary']) ?>
<?= Html::a('Delete', ['delete', 'id' => $model['job']->id], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
    ],
]) ?>

<?= DetailView::widget([
    'model' => $model['job'],
    'attributes' => [
        'id',
        'code',
        'name',
        'description:ntext',
        'requirement:ntext',
        'id_job_category',
        'batch',
        'date_start',
        'date_end',
        'id_job_step_last',
    ],
]) ?>