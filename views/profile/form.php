<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row">
    <div class="col-xs-8">    
<?php endif; ?>

<?php $form = ActiveForm::begin(); ?>

<?php switch ($category) :
    case 'basic': ?>
        <?= '' /*$form->field($model['applicant'], 'name')->textInput(['maxlength' => true])*/ ?>
        <?= $form->field($model['applicant'], 'name')->begin(); ?>
            <?= Html::activeLabel($model['applicant'], 'name', ['class' => 'col-xs-4 control-label']); ?>
        	<div class="col-xs-8">
	            <?= Html::activeTextInput($model['applicant'], 'name', ['class' => 'form-control', 'maxlength' => true/*, 'initial-value ng-model' => 'applicant.textinput'*/]); ?>
	            <?= Html::error($model['applicant'], 'name', ['class' => 'help-block']); ?>
        	</div>
        <?= $form->field($model['applicant'], 'name')->end(); ?>

        <?= ''/*$form->field($model['applicant'], 'nickname')->textInput(['maxlength' => true])*/ ?>
        <?= $form->field($model['applicant'], 'nickname')->begin(); ?>
            <?= Html::activeLabel($model['applicant'], 'nickname', ['class' => 'col-xs-4 control-label']); ?>
        	<div class="col-xs-8">
	            <?= Html::activeTextInput($model['applicant'], 'nickname', ['class' => 'form-control', 'maxlength' => true/*, 'initial-value ng-model' => 'applicant.textinput'*/]); ?>
	            <?= Html::error($model['applicant'], 'nickname', ['class' => 'help-block']); ?>
        	</div>
        <?= $form->field($model['applicant'], 'nickname')->end(); ?>

        <?= ''/*$form->field($model['applicant'], 'gender')->dropDownList([ 'f' => 'F', 'm' => 'M', ], ['prompt' => ''])*/ ?>
        <?= $form->field($model['applicant'], 'gender')->begin(); ?>
            <?= Html::activeLabel($model['applicant'], 'gender', ['class' => 'col-xs-4 control-label']); ?>
        	<div class="col-xs-8">
	            <?= Html::activeRadioList($model['applicant'], 'gender', $model['applicant']->getEnum('gender'), ['unselect' => null,
			        'item' => function($index, $label, $name, $checked, $value){
			            $disabled = in_array($value, ['val1', 'val2', '1']) ? true : false;

			            $radio = Html::radio($name, $checked, ['value' => $value, 'disabled' => $disabled]);
			            return Html::tag('div', Html::label($radio . '<i></i>' . $label), ['class' => 'radio']);
			        }]); ?>
	            <?= Html::error($model['applicant'], 'gender', ['class' => 'help-block']); ?>
        	</div>
        <?= $form->field($model['applicant'], 'gender')->end(); ?>

        <?= ''/*$form->field($model['applicant'], 'birthplace')->textInput(['maxlength' => true])*/ ?>
        <?= $form->field($model['applicant'], 'birthplace')->begin(); ?>
            <?= Html::activeLabel($model['applicant'], 'birthplace', ['class' => 'col-xs-4 control-label']); ?>
        	<div class="col-xs-8">
	            <?= Html::activeTextInput($model['applicant'], 'birthplace', ['class' => 'form-control', 'maxlength' => true/*, 'initial-value ng-model' => 'applicant.textinput'*/]); ?>
	            <?= Html::error($model['applicant'], 'birthplace', ['class' => 'help-block']); ?>
        	</div>
        <?= $form->field($model['applicant'], 'birthplace')->end(); ?>

        <?= ''/*$form->field($model['applicant'], 'birthday')->textInput()*/ ?>
        <?= $form->field($model['applicant'], 'birthday')->begin(); ?>
            <?= Html::activeLabel($model['applicant'], 'birthday', ['class' => 'col-xs-4 control-label']); ?>
        	<div class="col-xs-8">
	            <?= Html::activeTextInput($model['applicant'], 'birthday', ['class' => 'form-control', 'maxlength' => true/*, 'initial-value ng-model' => 'applicant.textinput'*/]); ?>
	            <?= Html::error($model['applicant'], 'birthday', ['class' => 'help-block']); ?>
        	</div>
        <?= $form->field($model['applicant'], 'birthday')->end(); ?>

        <?= ''/*$form->field($model['applicant'], 'religion')->dropDownList([ 'islam' => 'Islam', 'protestan' => 'Protestan', 'katholik' => 'Katholik', 'hindhu' => 'Hindhu', 'budha' => 'Budha', 'konghucu' => 'Konghucu', ], ['prompt' => ''])*/ ?>
        <?= $form->field($model['applicant'], 'religion')->begin(); ?>
            <?= Html::activeLabel($model['applicant'], 'religion', ['class' => 'col-xs-4 control-label']); ?>
        	<div class="col-xs-8">
	            <?= Html::activeRadioList($model['applicant'], 'religion', $model['applicant']->getEnum('religion'), ['unselect' => null,
			        'item' => function($index, $label, $name, $checked, $value){
			            $disabled = in_array($value, ['val1', 'val2', '1']) ? true : false;

			            $radio = Html::radio($name, $checked, ['value' => $value, 'disabled' => $disabled]);
			            return Html::tag('div', Html::label($radio . '<i></i>' . $label), ['class' => 'radio']);
			        }]); ?>
	            <?= Html::error($model['applicant'], 'religion', ['class' => 'help-block']); ?>
        	</div>
        <?= $form->field($model['applicant'], 'religion')->end(); ?>

        <?= ''/*$form->field($model['applicant'], 'status_marital')->dropDownList([ 'lajang' => 'Lajang', 'menikah' => 'Menikah', 'duda/janda' => 'Duda/janda', '' => '', ], ['prompt' => ''])*/ ?>
        <?= $form->field($model['applicant'], 'status_marital')->begin(); ?>
            <?= Html::activeLabel($model['applicant'], 'status_marital', ['class' => 'col-xs-4 control-label']); ?>
        	<div class="col-xs-8">
	            <?= Html::activeRadioList($model['applicant'], 'status_marital', $model['applicant']->getEnum('status_marital'), ['unselect' => null,
			        'item' => function($index, $label, $name, $checked, $value){
			            $disabled = in_array($value, ['val1', 'val2', '1']) ? true : false;

			            $radio = Html::radio($name, $checked, ['value' => $value, 'disabled' => $disabled]);
			            return Html::tag('div', Html::label($radio . '<i></i>' . $label), ['class' => 'radio']);
			        }]); ?>
	            <?= Html::error($model['applicant'], 'status_marital', ['class' => 'help-block']); ?>
        	</div>
        <?= $form->field($model['applicant'], 'status_marital')->end(); ?>

        <?= ''/*$form->field($model['applicant'], 'nationality')->textInput(['maxlength' => true])*/ ?>
        <?= $form->field($model['applicant'], 'nationality')->begin(); ?>
            <?= Html::activeLabel($model['applicant'], 'nationality', ['class' => 'col-xs-4 control-label']); ?>
        	<div class="col-xs-8">
	            <?= Html::activeTextInput($model['applicant'], 'nationality', ['class' => 'form-control', 'maxlength' => true/*, 'initial-value ng-model' => 'applicant.textinput'*/]); ?>
	            <?= Html::error($model['applicant'], 'nationality', ['class' => 'help-block']); ?>
        	</div>
        <?= $form->field($model['applicant'], 'nationality')->end(); ?>

        <?= ''/*$form->field($model['applicant'], 'number_identity')->textInput(['maxlength' => true])*/ ?>
        <?= $form->field($model['applicant'], 'number_identity')->begin(); ?>
            <?= Html::activeLabel($model['applicant'], 'number_identity', ['class' => 'col-xs-4 control-label']); ?>
        	<div class="col-xs-8">
	            <?= Html::activeTextInput($model['applicant'], 'number_identity', ['class' => 'form-control', 'maxlength' => true/*, 'initial-value ng-model' => 'applicant.textinput'*/]); ?>
	            <?= Html::error($model['applicant'], 'number_identity', ['class' => 'help-block']); ?>
        	</div>
        <?= $form->field($model['applicant'], 'number_identity')->end(); ?>
        <?php break; ?>

    <?php case 'physical': ?>
        <?= $form->field($model['applicant'], 'photo')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'height')->textInput() ?>

        <?= $form->field($model['applicant'], 'weight')->textInput() ?>

        <?= $form->field($model['applicant'], 'use_glassess')->textInput() ?>
        <?php break; ?>

    <?php case 'contact': ?>
        <?= $form->field($model['applicant'], 'home_phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'mobile_phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'email')->textInput(['maxlength' => true]) ?>
        <?php break; ?>
    
    <?php case 'address': ?>
        <?= $form->field($model['applicant'], 'permanent_full_address')->textarea(['rows' => 6]) ?>

        <?= $form->field($model['applicant'], 'permanent_city')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'permanent_province')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'permanent_country')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'permanent_postcode')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'present_full_address')->textarea(['rows' => 6]) ?>

        <?= $form->field($model['applicant'], 'present_city')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'present_province')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'present_country')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'present_postcode')->textInput(['maxlength' => true]) ?>
        <?php break; ?>
        
    <?php case 'family': ?>
        <h3 class="border-bottom">Father</h3>
        <?= $form->field($model['applicant'], 'father_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'father_birthday')->textInput() ?>

        <?= $form->field($model['applicant'], 'father_birthplace')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'father_occupation')->textInput(['maxlength' => true]) ?>

        <h3 class="border-bottom">Mother</h3>
        <?= $form->field($model['applicant'], 'mother_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'mother_birthday')->textInput() ?>

        <?= $form->field($model['applicant'], 'mother_birthplace')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'mother_occupation')->textInput(['maxlength' => true]) ?>

        <h3 class="border-bottom">Siblings</h3>
        <?= $form->field($model['applicant'], 'sibling_order')->textInput() ?>

        <?= $form->field($model['applicant'], 'sibling_total')->textInput() ?>

        <h3 class="border-bottom">Status</h3>
        <?= $form->field($model['applicant'], 'status_marital')->dropDownList([ 'lajang' => 'Lajang', 'menikah' => 'Menikah', 'duda/janda' => 'Duda/janda', '' => '', ], ['prompt' => '']) ?>
        
        <h3 class="border-bottom">Husband / Wife (if married)</h3>
        <?= $form->field($model['applicant'], 'mate_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'mate_birthday')->textInput() ?>

        <?= $form->field($model['applicant'], 'mate_birthplace')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model['applicant'], 'mate_occupation')->textInput(['maxlength' => true]) ?>

        <h3 class="border-bottom">Child (if any)</h3>
        <?= $form->field($model['applicant'], 'total_child')->textInput() ?>
        <?php break; ?>

    <?php case 'document': ?>
        <?= $form->field($model['applicant'], 'scan_kk')->textInput(['maxlength' => true]) ?>
        <?php break; ?>

    <?php case 'driving': ?>
        <?= $form->field($model['applicant'], 'sim_a')->textInput() ?>

        <?= $form->field($model['applicant'], 'sim_a_expired_date')->textInput() ?>

        <?= $form->field($model['applicant'], 'sim_b1')->textInput() ?>

        <?= $form->field($model['applicant'], 'sim_b1_expired_date')->textInput() ?>

        <?= $form->field($model['applicant'], 'sim_b2')->textInput() ?>

        <?= $form->field($model['applicant'], 'sim_b2_expired_date')->textInput() ?>

        <?= $form->field($model['applicant'], 'sim_c')->textInput() ?>

        <?= $form->field($model['applicant'], 'sim_c_expired_date')->textInput() ?>

        <?= $form->field($model['applicant'], 'sim_d')->textInput() ?>

        <?= $form->field($model['applicant'], 'sim_d_expired_date')->textInput() ?>
        <?php break; ?>

    <?php case 'about-me': ?>
    <?= $form->field($model['applicant'], 'about_me')->textarea(['rows' => 6]) ?>
        <?php break; ?>

    <?php default: ?>
        <?php break; ?>
<?php endswitch; ?>

<div class="form-group">
    <?= Html::submitButton($model['applicant']->isNewRecord ? 'Create' : 'Update', ['class' => $model['applicant']->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::resetButton('Reset', ['class' => 'btn btn-success']); ?>
</div>

<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>