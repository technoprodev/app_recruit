<?php

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/technosmart/js/job-category/list.js', ['depends' => 'technosmart\assets\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Id Parent</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; ID" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Name" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Description" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Id Parent" class="form-control no-border fs-12 f-normal"/></th>
        </tr>
    </thead>
</table>