<?php

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?= $this->render('list', [
    'model' => $model,
]) ?>

<br><br><hr><br>

<?= $this->render('form', [
    'model' => $model,
]) ?>