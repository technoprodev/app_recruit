<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row">
    <div class="col-xs-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model['job-category'], 'id')->textInput() ?>

<?= $form->field($model['job-category'], 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['job-category'], 'description')->textarea(['rows' => 6]) ?>

<?= $form->field($model['job-category'], 'id_parent')->textInput() ?>


<div class="form-group">
    <?= Html::submitButton($model['job-category']->isNewRecord ? 'Create' : 'Update', ['class' => $model['job-category']->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::resetButton('Reset', ['class' => 'btn btn-success']); ?>
</div>

<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>