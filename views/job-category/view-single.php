<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?= Html::a('Update', ['update', 'id' => $model['job-category']->id], ['class' => 'btn btn-primary']) ?>
<?= Html::a('Delete', ['delete', 'id' => $model['job-category']->id], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
    ],
]) ?>

<?= DetailView::widget([
    'model' => $model['job-category'],
    'attributes' => [
        'id',
        'name',
        'description:ntext',
        'id_parent',
    ],
]) ?>