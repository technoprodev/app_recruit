<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?= Html::a('Update', ['update', 'id' => $model['applicant']->id], ['class' => 'btn btn-primary']) ?>
<?= Html::a('Delete', ['delete', 'id' => $model['applicant']->id], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
    ],
]) ?>

<?= DetailView::widget([
    'model' => $model['applicant'],
    'attributes' => [
        'id',
        'id_user',
        'created_at',
        'updated_at',
        'name',
        'nickname',
        'gender',
        'birthplace',
        'birthday',
        'religion',
        'status_marital',
        'number_identity',
        'photo',
        'permanent_full_address:ntext',
        'permanent_city',
        'permanent_province',
        'permanent_country',
        'permanent_postcode',
        'present_full_address:ntext',
        'present_city',
        'present_province',
        'present_country',
        'present_postcode',
        'home_phone',
        'mobile_phone',
        'email:email',
        'father_name',
        'father_birthday',
        'father_birthplace',
        'father_occupation',
        'mother_name',
        'mother_birthday',
        'mother_birthplace',
        'mother_occupation',
        'sibling_order',
        'sibling_total',
        'mate_name',
        'mate_birthday',
        'mate_birthplace',
        'mate_occupation',
        'total_child',
        'scan_kk',
        'height',
        'weight',
        'use_glassess',
        'sim_a',
        'sim_a_expired_date',
        'sim_b1',
        'sim_b1_expired_date',
        'sim_b2',
        'sim_b2_expired_date',
        'sim_c',
        'sim_c_expired_date',
        'sim_d',
        'sim_d_expired_date',
        'about_me:ntext',
        'nationality',
    ],
]) ?>