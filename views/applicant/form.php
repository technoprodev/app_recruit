<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row">
    <div class="col-xs-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model['applicant'], 'id')->textInput() ?>

<?= $form->field($model['applicant'], 'id_user')->textInput() ?>

<?= $form->field($model['applicant'], 'created_at')->textInput() ?>

<?= $form->field($model['applicant'], 'updated_at')->textInput() ?>

<?= $form->field($model['applicant'], 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'nickname')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'gender')->dropDownList([ 'Female' => 'Female', 'Male' => 'Male', ], ['prompt' => '']) ?>

<?= $form->field($model['applicant'], 'birthplace')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'birthday')->textInput() ?>

<?= $form->field($model['applicant'], 'religion')->dropDownList([ 'Islam' => 'Islam', 'Protestan' => 'Protestan', 'Katholik' => 'Katholik', 'Hindhu' => 'Hindhu', 'Budha' => 'Budha', 'konghucu' => 'Konghucu', ], ['prompt' => '']) ?>

<?= $form->field($model['applicant'], 'status_marital')->dropDownList([ 'Lajang' => 'Lajang', 'Menikah' => 'Menikah', 'Duda/Janda' => 'Duda/Janda', ], ['prompt' => '']) ?>

<?= $form->field($model['applicant'], 'number_identity')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'photo')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'permanent_full_address')->textarea(['rows' => 6]) ?>

<?= $form->field($model['applicant'], 'permanent_city')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'permanent_province')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'permanent_country')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'permanent_postcode')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'present_full_address')->textarea(['rows' => 6]) ?>

<?= $form->field($model['applicant'], 'present_city')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'present_province')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'present_country')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'present_postcode')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'home_phone')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'mobile_phone')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'email')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'father_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'father_birthday')->textInput() ?>

<?= $form->field($model['applicant'], 'father_birthplace')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'father_occupation')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'mother_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'mother_birthday')->textInput() ?>

<?= $form->field($model['applicant'], 'mother_birthplace')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'mother_occupation')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'sibling_order')->textInput() ?>

<?= $form->field($model['applicant'], 'sibling_total')->textInput() ?>

<?= $form->field($model['applicant'], 'mate_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'mate_birthday')->textInput() ?>

<?= $form->field($model['applicant'], 'mate_birthplace')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'mate_occupation')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'total_child')->textInput() ?>

<?= $form->field($model['applicant'], 'scan_kk')->textInput(['maxlength' => true]) ?>

<?= $form->field($model['applicant'], 'height')->textInput() ?>

<?= $form->field($model['applicant'], 'weight')->textInput() ?>

<?= $form->field($model['applicant'], 'use_glassess')->textInput() ?>

<?= $form->field($model['applicant'], 'sim_a')->textInput() ?>

<?= $form->field($model['applicant'], 'sim_a_expired_date')->textInput() ?>

<?= $form->field($model['applicant'], 'sim_b1')->textInput() ?>

<?= $form->field($model['applicant'], 'sim_b1_expired_date')->textInput() ?>

<?= $form->field($model['applicant'], 'sim_b2')->textInput() ?>

<?= $form->field($model['applicant'], 'sim_b2_expired_date')->textInput() ?>

<?= $form->field($model['applicant'], 'sim_c')->textInput() ?>

<?= $form->field($model['applicant'], 'sim_c_expired_date')->textInput() ?>

<?= $form->field($model['applicant'], 'sim_d')->textInput() ?>

<?= $form->field($model['applicant'], 'sim_d_expired_date')->textInput() ?>

<?= $form->field($model['applicant'], 'about_me')->textarea(['rows' => 6]) ?>

<?= $form->field($model['applicant'], 'nationality')->textInput(['maxlength' => true]) ?>


<div class="form-group">
    <?= Html::submitButton($model['applicant']->isNewRecord ? 'Create' : 'Update', ['class' => $model['applicant']->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::resetButton('Reset', ['class' => 'btn btn-success']); ?>
</div>

<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>