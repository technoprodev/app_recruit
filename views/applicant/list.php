<?php

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/technosmart/js/applicant/list.js', ['depends' => 'technosmart\assets\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th>ID</th>
            <th>Id User</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Name</th>
            <th>Nickname</th>
            <th>Gender</th>
            <th>Birthplace</th>
            <th>Birthday</th>
            <th>Religion</th>
            <th>Status Marital</th>
            <th>Number Identity</th>
            <th>Photo</th>
            <th>Permanent Full Address</th>
            <th>Permanent City</th>
            <th>Permanent Province</th>
            <th>Permanent Country</th>
            <th>Permanent Postcode</th>
            <th>Present Full Address</th>
            <th>Present City</th>
            <th>Present Province</th>
            <th>Present Country</th>
            <th>Present Postcode</th>
            <th>Home Phone</th>
            <th>Mobile Phone</th>
            <th>Email</th>
            <th>Father Name</th>
            <th>Father Birthday</th>
            <th>Father Birthplace</th>
            <th>Father Occupation</th>
            <th>Mother Name</th>
            <th>Mother Birthday</th>
            <th>Mother Birthplace</th>
            <th>Mother Occupation</th>
            <th>Sibling Order</th>
            <th>Sibling Total</th>
            <th>Mate Name</th>
            <th>Mate Birthday</th>
            <th>Mate Birthplace</th>
            <th>Mate Occupation</th>
            <th>Total Child</th>
            <th>Scan Kk</th>
            <th>Height</th>
            <th>Weight</th>
            <th>Use Glassess</th>
            <th>Sim A</th>
            <th>Sim A Expired Date</th>
            <th>Sim B1</th>
            <th>Sim B1 Expired Date</th>
            <th>Sim B2</th>
            <th>Sim B2 Expired Date</th>
            <th>Sim C</th>
            <th>Sim C Expired Date</th>
            <th>Sim D</th>
            <th>Sim D Expired Date</th>
            <th>About Me</th>
            <th>Nationality</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; ID" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Id User" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Created At" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Updated At" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Name" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Nickname" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Gender" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Birthplace" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Birthday" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Religion" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Status Marital" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Number Identity" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Photo" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Permanent Full Address" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Permanent City" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Permanent Province" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Permanent Country" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Permanent Postcode" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Present Full Address" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Present City" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Present Province" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Present Country" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Present Postcode" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Home Phone" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Mobile Phone" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Email" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Father Name" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Father Birthday" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Father Birthplace" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Father Occupation" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Mother Name" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Mother Birthday" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Mother Birthplace" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Mother Occupation" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sibling Order" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sibling Total" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Mate Name" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Mate Birthday" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Mate Birthplace" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Mate Occupation" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Total Child" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Scan Kk" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Height" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Weight" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Use Glassess" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sim A" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sim A Expired Date" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sim B1" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sim B1 Expired Date" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sim B2" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sim B2 Expired Date" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sim C" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sim C Expired Date" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sim D" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Sim D Expired Date" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; About Me" class="form-control no-border fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Nationality" class="form-control no-border fs-12 f-normal"/></th>
        </tr>
    </thead>
</table>