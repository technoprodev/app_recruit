(function($) {
	// datatables
	if (typeof $.fn.dataTable === 'function') {
		$.fn.dataTableExt.oStdClasses.sWrapper = 'dataTables_wrapper ff-default';
		$.fn.dataTableExt.oStdClasses.sLength = 'dataTables_length form-group form-group-sm pull-left';
		$.fn.dataTableExt.oStdClasses.sLengthSelect = 'form-control';
		$.fn.dataTableExt.oStdClasses.sFilter = 'dataTables_filter form-group form-group-sm pull-right';
		$.fn.dataTableExt.oStdClasses.sFilterInput = 'form-control';
		$.fn.dataTableExt.oStdClasses.sInfo = 'dataTables_info pull-left';
		$.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate pull-right paging_';
		$.fn.dataTableExt.oStdClasses.sPageButtonActive = 'active';
		var el = $('.datatables');
		$.each(el, function() {
			var table = $(this).DataTable({
				"autoWidth": false,
				"deferRender": true,
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": fn.urlTo('applicant/datatables'),
					"type": "POST"
				},
				"columns": [
                    {'data': 'id'},
                    {'data': 'id_user'},
                    {'data': 'created_at'},
                    {'data': 'updated_at'},
                    {'data': 'name'},
                    {'data': 'nickname'},
                    {'data': 'gender'},
                    {'data': 'birthplace'},
                    {'data': 'birthday'},
                    {'data': 'religion'},
                    {'data': 'status_marital'},
                    {'data': 'number_identity'},
                    {'data': 'photo'},
                    {'data': 'permanent_full_address'},
                    {'data': 'permanent_city'},
                    {'data': 'permanent_province'},
                    {'data': 'permanent_country'},
                    {'data': 'permanent_postcode'},
                    {'data': 'present_full_address'},
                    {'data': 'present_city'},
                    {'data': 'present_province'},
                    {'data': 'present_country'},
                    {'data': 'present_postcode'},
                    {'data': 'home_phone'},
                    {'data': 'mobile_phone'},
                    {'data': 'email'},
                    {'data': 'father_name'},
                    {'data': 'father_birthday'},
                    {'data': 'father_birthplace'},
                    {'data': 'father_occupation'},
                    {'data': 'mother_name'},
                    {'data': 'mother_birthday'},
                    {'data': 'mother_birthplace'},
                    {'data': 'mother_occupation'},
                    {'data': 'sibling_order'},
                    {'data': 'sibling_total'},
                    {'data': 'mate_name'},
                    {'data': 'mate_birthday'},
                    {'data': 'mate_birthplace'},
                    {'data': 'mate_occupation'},
                    {'data': 'total_child'},
                    {'data': 'scan_kk'},
                    {'data': 'height'},
                    {'data': 'weight'},
                    {'data': 'use_glassess'},
                    {'data': 'sim_a'},
                    {'data': 'sim_a_expired_date'},
                    {'data': 'sim_b1'},
                    {'data': 'sim_b1_expired_date'},
                    {'data': 'sim_b2'},
                    {'data': 'sim_b2_expired_date'},
                    {'data': 'sim_c'},
                    {'data': 'sim_c_expired_date'},
                    {'data': 'sim_d'},
                    {'data': 'sim_d_expired_date'},
                    {'data': 'about_me'},
                    {'data': 'nationality'}
				],
				dom: '<"clearfix"lBf><"clearfix margin-bottom-10 scroll-x"rt><"clearfix"ip>',
				"lengthMenu": [[10, 25, 50, 100, -1], ['10 entries', '25 entries', '50 entries', '100 entries', "All entries"]],
				"orderCellsTop": true,
				// "ordering": false, //
				// "order": [], //
				// "order": [[1, 'asc'], [2, 'asc']], //

				buttons: {
					dom: {
						container: {
							className: 'dt-buttons pull-left'
						},
						button: {
							className: 'margin-left-5 btn btn-default btn-sm'
						}
					},
					buttons: [
						{
							extend: 'colvis',
							title: 'Data show/hide',
							text: 'Show/hide'
						},
						{
							extend: 'copy',
							title: 'Data export',
							text: 'Copy'
						},
						{
							extend: 'csv',
							title: 'Data export',
							text: 'Csv'
						},
						{
							extend: 'excel',
							title: 'Data export',
							text: 'Excel'
						},
						{
							extend: 'pdf',
							title: 'Data export',
							text: 'Pdf'
						},
						{
							extend: 'print',
							title: 'Data export',
							text: 'Print'
						}/*,
						{
							text: 'My button',
							action: function ( e, dt, node, config ) {
								alert( 'Button activated' );
							}
						},
						{
							extend: 'collection',
							text: 'Table control',
							autoClose: true,
							buttons: [
								{
									text: 'Toggle start date',
									action: function ( e, dt, node, config ) {
										dt.column( -2 ).visible( ! dt.column( -2 ).visible() );
									}
								},
								{
									text: 'Toggle salary',
									action: function ( e, dt, node, config ) {
										dt.column( -1 ).visible( ! dt.column( -1 ).visible() );
									}
								}
							]
						}*/
					]
				},
				language: {
					lengthMenu : "_MENU_",
					search: "",
					searchPlaceholder: "Search here",
					buttons: {
						copyTitle: 'Title',
						copyKeys: 'copy keys',
						copySuccess: {
							_: '%d rows copied',
							1: '1 row copied'
						}
					}
				},
				colReorder: true
			});

			var dtSearch = $('.dt-search', $(this));
			table.columns().every(function(index, table, column){
				var that = this;
				
				var timer = null;
				dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
					var val = {
						that : that,
						this : this,
					};

					clearTimeout(timer); 
	   				timer = setTimeout(function () {
						if(val.that.search() !== val.this.value){
							val.that.search( val.this.value ).draw();
						}
					}, 500);
				});
			});
		});
		// $('th', el).unbind('click.DT'); //
		// $('th', el).remove(); //
	}
})(jQuery);