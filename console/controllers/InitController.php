<?php
namespace console\controllers;

use yii\console\Controller;
use technosmart\models\Permission;

class InitController extends Controller
{
    public $dir = [
        'recruitment\controllers' => '@recruitment/controllers',
    ];

    public function actionIndex()
    {
        \Yii::$app->runAction('cache/flush-all');

        \Yii::$app->cache->cachePath = \Yii::getAlias('@recruitment/runtime/cache');
        \Yii::$app->runAction('cache/flush-all');

        if (Permission::initAllController($this->dir))
            return 0;
        else
            return 1;
    }
}