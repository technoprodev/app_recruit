<?php
namespace recruitment\models;

use Yii;

/**
 * This is the model class for table "applicant".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 * @property string $nickname
 * @property string $gender
 * @property string $birthplace
 * @property string $birthday
 * @property string $religion
 * @property string $status_marital
 * @property string $number_identity
 * @property string $photo
 * @property string $permanent_full_address
 * @property string $permanent_city
 * @property string $permanent_province
 * @property string $permanent_country
 * @property string $permanent_postcode
 * @property string $present_full_address
 * @property string $present_city
 * @property string $present_province
 * @property string $present_country
 * @property string $present_postcode
 * @property string $home_phone
 * @property string $mobile_phone
 * @property string $email
 * @property string $father_name
 * @property string $father_birthday
 * @property string $father_birthplace
 * @property string $father_occupation
 * @property string $mother_name
 * @property string $mother_birthday
 * @property string $mother_birthplace
 * @property string $mother_occupation
 * @property integer $sibling_order
 * @property integer $sibling_total
 * @property string $mate_name
 * @property string $mate_birthday
 * @property string $mate_birthplace
 * @property string $mate_occupation
 * @property integer $total_child
 * @property string $scan_kk
 * @property integer $height
 * @property integer $weight
 * @property integer $use_glassess
 * @property integer $sim_a
 * @property string $sim_a_expired_date
 * @property integer $sim_b1
 * @property string $sim_b1_expired_date
 * @property integer $sim_b2
 * @property string $sim_b2_expired_date
 * @property integer $sim_c
 * @property string $sim_c_expired_date
 * @property integer $sim_d
 * @property string $sim_d_expired_date
 * @property string $about_me
 * @property string $nationality
 */
class Applicant extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'applicant';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbr');
    }

    public function rules()
    {
        return [
            //id

            //id_user
            [['id_user'], 'required'],
            [['id_user'], 'integer'],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //name
            [['name'], 'string', 'max' => 64],

            //nickname
            [['nickname'], 'string', 'max' => 32],

            //gender
            [['gender'], 'string'],

            //birthplace
            [['birthplace'], 'string', 'max' => 64],

            //birthday
            [['birthday'], 'safe'],

            //religion
            [['religion'], 'string'],

            //status_marital
            [['status_marital'], 'string'],

            //number_identity
            [['number_identity'], 'string', 'max' => 64],

            //photo
            [['photo'], 'string', 'max' => 64],

            //permanent_full_address
            [['permanent_full_address'], 'string'],

            //permanent_city
            [['permanent_city'], 'string', 'max' => 64],

            //permanent_province
            [['permanent_province'], 'string', 'max' => 64],

            //permanent_country
            [['permanent_country'], 'string', 'max' => 64],

            //permanent_postcode
            [['permanent_postcode'], 'string', 'max' => 32],

            //present_full_address
            [['present_full_address'], 'string'],

            //present_city
            [['present_city'], 'string', 'max' => 64],

            //present_province
            [['present_province'], 'string', 'max' => 64],

            //present_country
            [['present_country'], 'string', 'max' => 64],

            //present_postcode
            [['present_postcode'], 'string', 'max' => 64],

            //home_phone
            [['home_phone'], 'string', 'max' => 32],

            //mobile_phone
            [['mobile_phone'], 'string', 'max' => 32],

            //email
            [['email'], 'string', 'max' => 32],

            //father_name
            [['father_name'], 'string', 'max' => 64],

            //father_birthday
            [['father_birthday'], 'safe'],

            //father_birthplace
            [['father_birthplace'], 'string', 'max' => 64],

            //father_occupation
            [['father_occupation'], 'string', 'max' => 64],

            //mother_name
            [['mother_name'], 'string', 'max' => 64],

            //mother_birthday
            [['mother_birthday'], 'safe'],

            //mother_birthplace
            [['mother_birthplace'], 'string', 'max' => 64],

            //mother_occupation
            [['mother_occupation'], 'string', 'max' => 64],

            //sibling_order
            [['sibling_order'], 'integer'],

            //sibling_total
            [['sibling_total'], 'integer'],

            //mate_name
            [['mate_name'], 'string', 'max' => 64],

            //mate_birthday
            [['mate_birthday'], 'safe'],

            //mate_birthplace
            [['mate_birthplace'], 'string', 'max' => 64],

            //mate_occupation
            [['mate_occupation'], 'string', 'max' => 64],

            //total_child
            [['total_child'], 'integer'],

            //scan_kk
            [['scan_kk'], 'string', 'max' => 64],

            //height
            [['height'], 'integer'],

            //weight
            [['weight'], 'integer'],

            //use_glassess
            [['use_glassess'], 'integer'],

            //sim_a
            [['sim_a'], 'integer'],

            //sim_a_expired_date
            [['sim_a_expired_date'], 'safe'],

            //sim_b1
            [['sim_b1'], 'integer'],

            //sim_b1_expired_date
            [['sim_b1_expired_date'], 'safe'],

            //sim_b2
            [['sim_b2'], 'integer'],

            //sim_b2_expired_date
            [['sim_b2_expired_date'], 'safe'],

            //sim_c
            [['sim_c'], 'integer'],

            //sim_c_expired_date
            [['sim_c_expired_date'], 'safe'],

            //sim_d
            [['sim_d'], 'integer'],

            //sim_d_expired_date
            [['sim_d_expired_date'], 'safe'],

            //about_me
            [['about_me'], 'string'],

            //nationality
            [['nationality'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'name' => 'Name',
            'nickname' => 'Nickname',
            'gender' => 'Gender',
            'birthplace' => 'Birthplace',
            'birthday' => 'Birthday',
            'religion' => 'Religion',
            'status_marital' => 'Status Marital',
            'number_identity' => 'Number Identity',
            'photo' => 'Photo',
            'permanent_full_address' => 'Permanent Full Address',
            'permanent_city' => 'Permanent City',
            'permanent_province' => 'Permanent Province',
            'permanent_country' => 'Permanent Country',
            'permanent_postcode' => 'Permanent Postcode',
            'present_full_address' => 'Present Full Address',
            'present_city' => 'Present City',
            'present_province' => 'Present Province',
            'present_country' => 'Present Country',
            'present_postcode' => 'Present Postcode',
            'home_phone' => 'Home Phone',
            'mobile_phone' => 'Mobile Phone',
            'email' => 'Email',
            'father_name' => 'Father Name',
            'father_birthday' => 'Father Birthday',
            'father_birthplace' => 'Father Birthplace',
            'father_occupation' => 'Father Occupation',
            'mother_name' => 'Mother Name',
            'mother_birthday' => 'Mother Birthday',
            'mother_birthplace' => 'Mother Birthplace',
            'mother_occupation' => 'Mother Occupation',
            'sibling_order' => 'Sibling Order',
            'sibling_total' => 'Sibling Total',
            'mate_name' => 'Mate Name',
            'mate_birthday' => 'Mate Birthday',
            'mate_birthplace' => 'Mate Birthplace',
            'mate_occupation' => 'Mate Occupation',
            'total_child' => 'Total Child',
            'scan_kk' => 'Scan Kk',
            'height' => 'Height',
            'weight' => 'Weight',
            'use_glassess' => 'Use Glassess',
            'sim_a' => 'Sim A',
            'sim_a_expired_date' => 'Sim A Expired Date',
            'sim_b1' => 'Sim B1',
            'sim_b1_expired_date' => 'Sim B1 Expired Date',
            'sim_b2' => 'Sim B2',
            'sim_b2_expired_date' => 'Sim B2 Expired Date',
            'sim_c' => 'Sim C',
            'sim_c_expired_date' => 'Sim C Expired Date',
            'sim_d' => 'Sim D',
            'sim_d_expired_date' => 'Sim D Expired Date',
            'about_me' => 'About Me',
            'nationality' => 'Nationality',
        ];
    }
}
