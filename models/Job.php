<?php
namespace recruitment\models;

use Yii;

/**
 * This is the model class for table "job".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property string $requirement
 * @property integer $id_job_category
 * @property integer $batch
 * @property string $date_start
 * @property string $date_end
 * @property integer $id_job_step_last
 */
class Job extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'job';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbr');
    }

    public function rules()
    {
        return [
            //id

            //code
            [['code'], 'string', 'max' => 64],

            //name
            [['name'], 'string', 'max' => 64],

            //description
            [['description'], 'string'],

            //requirement
            [['requirement'], 'string'],

            //id_job_category
            [['id_job_category'], 'integer'],

            //batch
            [['batch'], 'integer'],

            //date_start
            [['date_start'], 'safe'],

            //date_end
            [['date_end'], 'safe'],

            //id_job_step_last
            [['id_job_step_last'], 'integer'],
            [['id_job_step_last'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'description' => 'Description',
            'requirement' => 'Requirement',
            'id_job_category' => 'Id Job Category',
            'batch' => 'Batch',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'id_job_step_last' => 'Id Job Step Last',
        ];
    }
}
