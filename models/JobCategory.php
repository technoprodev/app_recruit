<?php
namespace recruitment\models;

use Yii;

/**
 * This is the model class for table "job_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $id_parent
 */
class JobCategory extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'job_category';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbr');
    }

    public function rules()
    {
        return [
            //id

            //name
            [['name'], 'string', 'max' => 64],

            //description
            [['description'], 'string'],

            //id_parent
            [['id_parent'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'id_parent' => 'Id Parent',
        ];
    }
}
