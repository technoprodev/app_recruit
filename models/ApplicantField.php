<?php
namespace recruitment\models;

use Yii;

/**
 * This is the model class for table "applicant_field".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_category
 */
class ApplicantField extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'applicant_field';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbr');
    }

    public function rules()
    {
        return [
            //id

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],

            //id_category
            [['id_category'], 'required'],
            [['id_category'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'id_category' => 'Id Category',
        ];
    }
}
