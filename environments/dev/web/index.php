<?php
defined('YII_ENV') or define('YII_ENV', 'dev');
defined('YII_DEBUG') or YII_ENV != 'dev' or define('YII_DEBUG', true);

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../technosmart/config/bootstrap.php');
require(__DIR__ . '/../recruitment/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../technosmart/config/main.php'),
    require(__DIR__ . '/../technosmart/config/main-local.php'),
    require(__DIR__ . '/../recruitment/config/main.php'),
    require(__DIR__ . '/../recruitment/config/main-local.php')
);

$application = new yii\web\Application($config);
$application->run();